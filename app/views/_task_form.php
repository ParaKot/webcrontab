<form method="post" action="/?action=add">
    <div class="form-group">
        <label for="command">Команда</label>
        <input type="text" name="command"" class="form-control" id="command" placeholder="">
    </div>
    <div class="form-group">
        <label for="schedule">Расписание</label>
        <input type="text" name="schedule" class="form-control" id="schedule" placeholder="* * * * *">
    </div>
    <button type="submit" class="btn btn-primary">Добавить</button>
</form>