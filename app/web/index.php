<?php

include_once ("../../config.php");

// Определяем место где лежат файлы классов, которые будем загружать
define('DIR',  __DIR__ );
define('APP',  DIR.'/../..');

// Устаревший вариант
function __autoload($class) {
    include APP. '/'. $class . '.php';
}

// Актуальный вариант с безымянной функцией
spl_autoload_register(function ($classname) {

    if (file_exists(APP. '/' . $classname . '.php')) {
        include APP . '/' . $classname . '.php';
    }
});

use app\controllers\App;

$app = new App();

if(isset($_GET['action'])) {
    if(is_callable([$app, $_GET['action']])) {
        $app->{$_GET['action']}();
    }
    else {
        header("HTTP/1.0 404 Not Found");
    }
}
else {
    echo $app->index();
}
