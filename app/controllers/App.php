<?php

namespace app\controllers;

use app\models\Task;
use PDO;
use PDOException;

class App
{
    public $db;
    public $taskModel;

    // Connect ti MySQL
    public function __construct() {
        try {
            $this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASSWORD);
        } catch(PDOException  $e ){
            echo "Error: ".$e;
        }

        $this->taskModel = new Task($this->db);
    }

    public function index()
    {
        $tasks = $this->taskModel->getAll();

        require_once ('../views/tasks.php');
    }

    public function delete()
    {
        $id = (integer) $_GET['id'];

        $this->taskModel->deleteTask($id);

        header("location: /");
    }

    public function add()
    {

        $data['command'] = $_POST['command'];
        $data['schedule'] = $_POST['schedule'];

        $this->taskModel->saveTask($data);

        header("location: /");
    }
}