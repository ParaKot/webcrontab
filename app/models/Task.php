<?php

namespace app\models;

class Task {

    private $db;

    /**
     * Task constructor.
     * @param $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $stm = $this->db->query("SELECT  * FROM `tasks`");
        $tasks = $stm->fetchAll(\PDO::FETCH_ASSOC);
        return $tasks;
    }

    public function deleteTask($id)
    {
        $sql = "DELETE FROM tasks WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
    }

    public function saveTask($data)
    {
        $sql = "INSERT INTO tasks(script_path,script_exectime) VALUES (
            :command, 
            :schedule)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindParam(':command', $data['command'], \PDO::PARAM_STR);
        $stmt->bindParam(':schedule', $data['schedule'], \PDO::PARAM_STR);

        $stmt->execute();
    }
}