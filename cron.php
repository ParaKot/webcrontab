<?php

include_once ("config.php");

class Cron {

    private $db;
    private $exec;
    private $rundatetime;

    // Connect to MySQL
    public function __construct() {
        try {
            $this->db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASSWORD);
        } catch(PDOException  $e ){
            echo "Error: ".$e;
        }
    }
    
    /**
     * Run main application
     */
    public function run()
    {
        while(true){
            $this->runTasks();
            sleep(60);
        }
    }

    /**
     * Get and run tasks
     */
    public function runTasks()
    {
        $this->rundatetime = date("U");
        $stm = $this->db->query("SELECT  * FROM `tasks`");
        $tasks = $stm->fetchAll(PDO::FETCH_ASSOC);

        // Clear shell command line
        $this->exec = null;
        
        foreach ($tasks as $task) {
            $exectime = $this->checkTime($task['script_exectime']);

            if($exectime == true) {
                echo "Command: " . $task['script_path'] . ' ' . $task['script_exectime'] . "\n";
                $this->addTast($task);
            }
        }
        
        if(count($this->exec)) {
            $exec = implode(";", $this->exec);
            echo "Exec commend: " . $exec . "\n";
            $result = system($exec);
            $this->exec = null;
            echo $result;
        }
    }

    /**
     * @param $task
     */
    public function addTast($task)
    {
        $this->exec[] = $task['script_path'] . ' > /dev/null ';
    }


    /**
     * @param $crontab
     * @param null $time
     * @return mixed
     */
    public function checkTime($crontab, $time = null) {
        if(is_null($time))
            $time = date("Y-m-d H:i:s");

        // Get current minute, hour, day, month, weekday
        $time = explode(' ', date('i G j n w', strtotime($time)));

        // Split crontab by space
        $crontab = explode(' ', $crontab);
        // Foreach part of crontab
        foreach ($crontab as $k => &$v) {
            // Remove leading zeros to prevent octal comparison, but not if number is already 1 digit
            $time[$k] = preg_replace('/^0+(?=\d)/', '', $time[$k]);
            // 5,10,15 each treated as seperate parts
            $v = explode(',', $v);
            // Foreach part we now have
            foreach ($v as &$v1) {
                // Do preg_replace with regular expression to create evaluations from crontab
                $v1 = preg_replace(
                // Regex
                    array(
                        // *
                        '/^\*$/',
                        // 5
                        '/^\d+$/',
                        // 5-10
                        '/^(\d+)\-(\d+)$/',
                        // */5
                        '/^\*\/(\d+)$/'
                    ),
                    // Evaluations
                    // trim leading 0 to prevent octal comparison
                    array(
                        // * is always true
                        'true',
                        // Check if it is currently that time,
                        $time[$k] . '===\0',
                        // Find if more than or equal lowest and lower or equal than highest
                        '(\1<=' . $time[$k] . ' and ' . $time[$k] . '<=\2)',
                        // Use modulus to find if true
                        $time[$k] . '%\1===0'
                    ),
                    // Subject we are working with
                    $v1
                );
            }
            // Join 5,10,15 with `or` conditional
            $v = '(' . implode(' or ', $v) . ')';
        }
        // Require each part is true with `and` conditional
        $crontab = implode(' and ', $crontab);
        // Evaluate total condition to find if true
        return eval('return ' . $crontab . ';');
    }
}

// Run cron
$app = new Cron();
$app->run();
